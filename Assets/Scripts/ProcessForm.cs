﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProcessForm : MonoBehaviour
{
	private static ProcessForm _instance;

	public static ProcessForm Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	//UI
	public InputField firstNameText;
	public InputField emailText;
	public InputField phoneText;
    public Dropdown sizeTxt;

	//Strings
	public string firstName;
	public string email;
	public string phone;
    public string size;

    [SerializeField]
    public int userID;
    public int highscore;


    JSONParser jp = new JSONParser();

    LoggingManager logger = new LoggingManager();

    private void Start()
    {
        size = sizeTxt.options[0].text;
    }


    public void SetInputFieldsEnglish(string _firstname, string _lastname, string _email)
	{
		firstNameText.text = _firstname;
		emailText.text = _email;
		firstName = _firstname;
	}


	//Add first name
	public void FirstNameTextChanged ()
	{
		firstName = firstNameText.text.ToString ();
	}


	//Add email
	public void EmailTextChanged ()
	{
		email = emailText.text.ToString ();
	}

	//Add phone
	public void PhoneTextChanged ()
	{
		phone = phoneText.text.ToString ();
	}


    //Add title
    public void SizeTextChanged(int index)
    {
        size = sizeTxt.options[index].text;
    }


    public void ResetForm()
	{
		firstNameText.text = "";
		emailText.text = "";
		phoneText.text = "";
        firstName = "";
        email = "";
        phone = "";
        size = sizeTxt.options[0].text;

    }

    public void SubmitToDatabase()
	{
        try
        {
            string returnString = DatabaseConnection.Instance.SubmitForm(firstName, email, phone, WebCamTexturePlay.Instance.storedPhotoPath, size);
            print(returnString);
            string message = "";
            jp.FindUserID(returnString, ref userID, ref message);
            MainManager.Instance.AlertError(message);
            logger.WriteToLogFile("User id - "+ userID.ToString());

            if(message != "Success")
            {
                //print("Problem");
            }

        }
        catch (System.Exception e)
        {
            MainManager.Instance.AlertError("Try Clicking photo again!");
            MainManager.Instance.OpenClickPhotoPanel();
            logger.WriteToLogFile(e.ToString() + "Error submitting details");

        }
    }


    public void CheckDataFields()
    {
        if(firstNameText.text != "" && firstName != "" && emailText.text != "" && email != "" && phoneText.text != "" && phone!= "")
        {
            logger.WriteToLogFile("*******************STARTING NEW USER********************");
            MainManager.Instance.OpenClickPhotoPanel();
        } else
        {
            MainManager.Instance.AlertError("Some of the fields are empty, Try again");
        }
    }
}
