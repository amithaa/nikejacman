﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;

public class VideoController : MonoBehaviour
{

	private static VideoController _instance;

	public static VideoController Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	//Videoplayer & audio
	public VideoPlayer videoPlayer;
	private AudioSource audioSource;

	//Raw image
	public RawImage ri;


	void Start ()
	{
		PlayVideo ();
	}

	public void PlayVideo ()
	{
		StartCoroutine (StartVideo ());
	}


	/// <summary>
	/// Play the video
	/// </summary>
	IEnumerator StartVideo ()
	{
		audioSource = videoPlayer.gameObject.GetComponent<AudioSource> ();

//		videoPlayer.source = VideoSource.Url;
//		videoPlayer.url = Application.dataPath + "/Video/video.mp4";//"http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";

		//Set Audio Output to AudioSource
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

		//Assign the Audio from Video to AudioSource to be played
		videoPlayer.EnableAudioTrack (0, true);
		videoPlayer.SetTargetAudioSource (0, audioSource);
		//audioSource.volume = 0.1f;
		videoPlayer.controlledAudioTrackCount = 1;

		//Wait until video is prepared
		WaitForSeconds waitTime = new WaitForSeconds (1);
		while (!videoPlayer.isPrepared) {
			yield return waitTime;
			break;
		}

		//audioSource.volume=0.1f;

		ri.texture = videoPlayer.texture;
		//Play Video
		videoPlayer.Play ();
		//Play Sound
		audioSource.Play ();

		while (videoPlayer.isPlaying) {
			//Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
			yield return null;
		}

	}

	//
	//	void EndReached(UnityEngine.Video.VideoPlayer vp)
	//	{
	//
	//		StopVideo ();
	//	}

	//	public void StopVideo()
	//	{
	//
	//		videoPlayer.Stop ();
	//
	////		audioSource.Stop ();
	//
	//	}

}