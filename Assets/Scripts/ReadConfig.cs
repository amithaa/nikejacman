﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ReadConfig {


	/// <summary>
	/// Get the contents of config file based on the id
	/// </summary>
	/// <param name="id">the key to search for</param>
	public static string[] GetLines (string id) {
		ArrayList lines = new ArrayList();
		string line;
		string file_path = Application.dataPath + "/config.txt";
		StreamReader inp_stm = new StreamReader(file_path);
		string lineID = "[" + id + "]";
		bool match = false;
		while((line = inp_stm.ReadLine()) != null) {
			if (match) {
				if (line.StartsWith("[")) {
					break;
				}
				if (line.Length > 0) {
					lines.Add(line);
				}
			}
			else if (line.StartsWith(lineID)) {
				match = true;
			}
		}
		inp_stm.Close();
		if (lines.Count > 0) {
			return (string[])lines.ToArray(typeof(string));
		}
		return new string[0];
	}
}
