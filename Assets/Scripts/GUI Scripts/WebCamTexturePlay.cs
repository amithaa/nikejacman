﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;


public class WebCamTexturePlay : MonoBehaviour
{
	private static WebCamTexturePlay _instance;

	public static WebCamTexturePlay Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}


	WebCamTexture webCamTexture;
	public RawImage finalRawimage;
	private bool camAvailable;
	public AspectRatioFitter finalFit;

	public string storedPhotoPath;
	string path;
    
	public Text debugText;
	public GameObject frameFinal;
	public GameObject clickButtonFinal;
    public Text countdown;

    LoggingManager logger = new LoggingManager();




    public byte[] bytes;

	public string fileName;

	void Start ()
	{
		path = Application.dataPath + "\\Faces\\";

		//Checking for cameras
		WebCamDevice[] devices = WebCamTexture.devices;

		if (devices.Length == 0) {
			Debug.Log ("No camera detected");
            MainManager.Instance.AlertError ("No camera detected");
            logger.WriteToLogFile("No Camera detected");
            MainManager.Instance.AlertError("Error adding user score");

            camAvailable = false;
			return;
		}

		for (int i = 0; i < devices.Length; ++i) {
			if (devices [i].isFrontFacing) {
				webCamTexture = new WebCamTexture (devices [i].name, Screen.width, Screen.height);
                print(devices[i].name);
			}

			if (webCamTexture == null) {
				Debug.Log ("No back cam");
                MainManager.Instance.AlertError ("No camera detected");
                logger.WriteToLogFile("No Camera detected");

                return;
			}
		}
			
	}

	void Update ()
	{
		//Adjust the camera on the device
		if (!camAvailable)
			return;
		float ratio = (float)webCamTexture.width / (float)webCamTexture.height;
		finalFit.aspectRatio = ratio;

		float scaleY = webCamTexture.videoVerticallyMirrored ? -1f : 1f;
		finalRawimage.rectTransform.localScale = new Vector3 (1f, scaleY, 1f);

		int orient = -webCamTexture.videoRotationAngle;
		finalRawimage.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);
	}


	//Start the camera
	public void StartCamera ()
	{
		camAvailable = true;
		//Set the raw texture to camera
		finalRawimage.texture = webCamTexture;
        webCamTexture.Play ();
	}


	//Stop the camera
	public void StopCamera ()
	{
		camAvailable = true;
		webCamTexture.Stop ();
	}


	//Capture the screenshot
	public void CapturePhoto ()
	{
		StartCoroutine (TakePhoto ());
	}


	private Texture2D ScaleTexture (Texture2D source, int targetWidth, int targetHeight)
	{
		Texture2D result = new Texture2D (targetWidth, targetHeight, source.format, true);
		Color[] rpixels = result.GetPixels (0);
		float incX = ((float)1 / source.width) * ((float)source.width / targetWidth);
		float incY = ((float)1 / source.height) * ((float)source.height / targetHeight);
		for (int px = 0; px < rpixels.Length; px++) {
			rpixels [px] = source.GetPixelBilinear (incX * ((float)px % targetWidth),
				incY * ((float)Mathf.Floor (px / targetWidth)));
		}
		result.SetPixels (rpixels, 0);
		result.Apply ();
		return result;
	}


	//Capture the screenshot
	IEnumerator TakePhoto ()
	{
		clickButtonFinal.SetActive (false);

        countdown.text = "3";
        yield return new WaitForSeconds(1.0f);

        countdown.text = "2";
        yield return new WaitForSeconds(1.0f);

        countdown.text = "1";
        yield return new WaitForSeconds(1.0f);
        frameFinal.SetActive(false);
        countdown.text = "";

        yield return new WaitForEndOfFrame (); 
		try {
			fileName = System.DateTime.Now.ToString ("yyyyMMddHHmmssfff") + ".jpg";
			storedPhotoPath = path + fileName;
            logger.WriteToLogFile(storedPhotoPath);

            //Texture2D photo = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);

            Texture2D photo = new Texture2D(540, 540, TextureFormat.RGB24, false);

            //photo.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);

            photo.ReadPixels(new Rect(690, 270, 540, 540), 0, 0);
            photo.Apply ();

			//Texture2D newScreenshot = ScaleTexture (photo, 540, 540);

			//Encode to a PNG/JPG
			bytes = photo.EncodeToJPG ();


			File.WriteAllBytes (storedPhotoPath, bytes);

		} catch (System.Exception e) {
			print (e);
            MainManager.Instance.AlertError (e.ToString());
            logger.WriteToLogFile(e.ToString());

            storedPhotoPath = "";
			fileName = "";
		}


		frameFinal.SetActive (true);
		clickButtonFinal.SetActive (true);
        MainManager.Instance.OpenReviewPhotoPanel();
        EditAndReviewPhoto.Instance.LoadPhoto();

	}


    public void SkipPhoto()
    {
        storedPhotoPath = path + "default.png";
        MainManager.Instance.OpenReviewPhotoPanel();
        EditAndReviewPhoto.Instance.LoadPhoto();
    }
}