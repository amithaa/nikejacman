﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class EditAndReviewPhoto : MonoBehaviour
{

	private static EditAndReviewPhoto _instance;

	public static EditAndReviewPhoto Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public RawImage displayPhotoPanel;
    

	public void LoadPhoto ()
	{
		string filePath = WebCamTexturePlay.Instance.storedPhotoPath;
		Texture2D tex = null;
		byte[] fileData;
		print (filePath);
		if (File.Exists (@filePath)) {
			fileData = File.ReadAllBytes (filePath);
			tex = new Texture2D (2, 2);
			tex.LoadImage (fileData); //..this will auto-resize the texture dimensions.
			displayPhotoPanel.texture = tex;
		} else
        {
            MainManager.Instance.AlertError("Could not load image");
        }
	}
		
}
