﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{
	private static TimerScript _instance;

	public static TimerScript Instance { get { return _instance; } }

	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public Text timerText;
	public float startTime;
	public bool timerFinished = true;

	public float timeLimit;

	public bool paused = false;

	public float totalTime = 0.0f;

    private ScoreManager SM;
    private GameGUINavigation GUINav;

    private void Start()
    {
        SM = GameObject.Find("Game Manager").GetComponent<ScoreManager>();
        GUINav = GameObject.Find("UI Manager").GetComponent<GameGUINavigation>();
        StartTimer();
    }


    public void StartTimer ()
	{
		timerFinished = false;
		startTime = Time.time;
		totalTime = 0.0f;

	}

	public void FinishTimer ()
	{
		timerFinished = true;
		timerText.text = "";

        Debug.Log("Treshold for High Score: " + SM.LowestHigh());
        //if (GameManager.score >= SM.LowestHigh())
        //    GUINav.getScoresMenu();
        //else
            GUINav.H_ShowGameOverScreen();
    }
	
	// Update is called once per frame
	void Update ()
	{
		if (timerFinished)
			return;

		if (!paused)
			totalTime += Time.deltaTime;

        //		float t = Time.time - startTime;
        if (totalTime >= timeLimit)
        {
            FinishTimer();
        }
        else
        {
            string seconds = (Mathf.Ceil(timeLimit - totalTime)).ToString();
            timerText.text = seconds;
        }
	}
}
