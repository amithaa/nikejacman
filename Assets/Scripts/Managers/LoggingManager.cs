﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoggingManager {

	/// <summary>
	/// Write to the log file
	/// </summary>
	/// <param name="message">Message to write</param>
	public void WriteToLogFile(string message) {

		using (System.IO.StreamWriter logFile = new System.IO.StreamWriter (Application.dataPath + "\\logfile.txt",true)) {

			logFile.WriteLine (message);
		}
	}
}
