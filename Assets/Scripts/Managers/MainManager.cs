﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class MainManager : MonoBehaviour
{

    private static MainManager _instance;

    public static MainManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public GameObject HighScorePanel;
    public GameObject RegisterPanel;
    public GameObject ClickPhotoPanel;
    public GameObject ReviewPhotoPanel;
    public GameObject ThankYouPanel;


    //Error display
    public Text debugText;

    [SerializeField]
    float timeleft = 60.0f;
    [SerializeField]
    bool timerStarted = false;


    VirtualKeyboard vk = new VirtualKeyboard();

    public GameObject USShoePanel;
    public GameObject EUShoePanel;
    public ToggleGroup USParent;
    public ToggleGroup EUParent;

    public GameObject debugBG;


    public AudioSource audio1;
    public AudioClip startSound;

    bool ready = true;

    public void PlaySound(AudioClip audio)
    {
        audio1.clip = audio;
        audio1.Play();
    }

    // Start is called before the first frame update
    void Start()
    {
        Application.runInBackground = true;
    }

    public void OpenHighScorePanel()
    {
        HighScorePanel.SetActive(true);
        RegisterPanel.SetActive(false);
        ClickPhotoPanel.SetActive(false);
        ReviewPhotoPanel.SetActive(false);
        ThankYouPanel.SetActive(false);
    }

    public void OpenRegisterPanel()
    {
        HighScorePanel.SetActive(false);
        RegisterPanel.SetActive(true);
        ClickPhotoPanel.SetActive(false);
        ReviewPhotoPanel.SetActive(false);
        ThankYouPanel.SetActive(false);
    }


    public void OpenClickPhotoPanel()
    {
        HighScorePanel.SetActive(false);
        RegisterPanel.SetActive(false);
        ClickPhotoPanel.SetActive(true);
        ReviewPhotoPanel.SetActive(false);
        ThankYouPanel.SetActive(false);

        WebCamTexturePlay.Instance.StartCamera();
    }

    public void OpenReviewPhotoPanel()
    {
        HighScorePanel.SetActive(false);
        RegisterPanel.SetActive(false);
        ClickPhotoPanel.SetActive(false);
        ReviewPhotoPanel.SetActive(true);
        ThankYouPanel.SetActive(false);

    }

    public void OpenThankYouPanel()
    {
        HighScorePanel.SetActive(false);
        RegisterPanel.SetActive(false);
        ClickPhotoPanel.SetActive(false);
        ReviewPhotoPanel.SetActive(false);
        ThankYouPanel.SetActive(true);
        WebCamTexturePlay.Instance.StopCamera();


    }


    /// <summary>
	/// Display thanks
	/// </summary>
	public void SayThanks()
    {
        AlertError("Thank you for registering");
        StartCoroutine(DisableThankYouPanel());
    }


    /// <summary>
    /// Disable thanks
    /// </summary>
    IEnumerator DisableThankYouPanel()
    {
        yield return new WaitForSeconds(5.0f);

        RestartScene();

    }



    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    void Update()
    {

        if(Input.GetKeyDown(KeyCode.A) && ready)
        {
            ready = false;
            PlaySound(startSound);
            OpenRegisterPanel();
            //SceneManager.LoadScene("game");
            StartTimer();

        }
        if (!ready)
        {
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                StartTimer();
            }

            if (Input.GetMouseButtonDown(0))
            {
                StartTimer();
            }
        }

        if (timerStarted)
        {
            timeleft -= Time.deltaTime;
            if (timeleft < 0)
            {
                RestartScene();
            }
        }
    }

    public void StartTimer()
    {
        timerStarted = true;
        timeleft = 60.0f;
    }


    public void OpenKeyboard()
    {
        //print("Opening");
        vk.ShowTouchKeyboard();

    }

    public void CloseKeyboard()
    {
        vk.HideTouchKeyboard();
       // print("Clos\ting");

    }


    public void USBtnPressed()
    {
        USShoePanel.SetActive(true);
        EUShoePanel.SetActive(false);
    }


    public void EUBtnPressed()
    {
        USShoePanel.SetActive(false);
        EUShoePanel.SetActive(true);
    }


    public void ToggleUS()
    {
        try
        {
            ProcessForm.Instance.size = "US - " + USParent.ActiveToggles().First().GetComponentInChildren<Text>().text.ToString();
        } catch(System.Exception)
        {

        }
    }


    public void ToggleEU()
    {
        try
        {
            ProcessForm.Instance.size = "EU - " + EUParent.ActiveToggles().First().GetComponentInChildren<Text>().text.ToString();
        }
        catch (System.Exception)
        {

        }
    }

    /// <summary>
    /// Display error message
    /// </summary>
    public void AlertError(string errorText)
    {
        debugBG.SetActive(true);
        debugText.gameObject.SetActive(true);
        debugText.text = errorText.ToUpper();
        StartCoroutine(DisableErrorText());
    }


    /// <summary>
    /// Disable error message
    /// </summary>
    IEnumerator DisableErrorText()
    {
        yield return new WaitForSeconds(2.0f);
        debugText.text = "";
        debugText.gameObject.SetActive(false);
        debugBG.SetActive(false);

    }


}
