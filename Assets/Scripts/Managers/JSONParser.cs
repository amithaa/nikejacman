﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class JSONParser
{

    /// <summary>
    /// Parse and Return User id
    /// </summary>
    /// <param name="_jsonstring">The string message</param>
    /// <param name="userid">User ID</param>
    public void FindUserID(string _jsonString, ref int userid, ref string message)
    {
        var N = JSON.Parse(_jsonString);
        try
        {
            userid = int.Parse(N["id"]);
            message = N["message"];
        }
        catch
        {
            userid = 0;
        }

    }


    /// <summary>
	/// Parse and Return User id
	/// </summary>
	/// <param name="_jsonstring">The string message</param>
	/// <param name="userid">User ID</param>
	public void FindHighScore(string _jsonString, ref int highscore)
    {
        var N = JSON.Parse(_jsonString);
        try
        {
            highscore = int.Parse(N[0]["score"]);
        }
        catch
        {
            highscore = 0;
        }

    }


}
