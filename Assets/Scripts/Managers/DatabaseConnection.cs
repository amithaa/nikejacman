﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.IO;

public class DatabaseConnection : MonoBehaviour
{
	private static DatabaseConnection _instance;

	public static DatabaseConnection Instance { get { return _instance; } }


	private void Awake ()
	{
		if (_instance != null && _instance != this) {
			Destroy (this.gameObject);
		} else {
			_instance = this;
		}
	}

	public string ipaddress	 = "http://192.168.1.102/nike/api/";

    LoggingManager logger = new LoggingManager();

    private void Start()
    {
        //IP
        try
        {
            string[] configMessage = ReadConfig.GetLines("IpAddress");
            ipaddress = configMessage[0];
        }
        catch
        {
            ipaddress = "http://192.168.1.102/nike/api/";
            string errorMsg = "Config error: Could not load Machine ID";
            logger.WriteToLogFile(errorMsg);
        }

    }


    /// <summary>
    /// Submit user details
    /// </summary>
    public string SubmitForm (string _firstname, string _email, string _number, string imagepath, string _size)
	{
		try {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create (ipaddress + "adduser.php");
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Headers.Add ("Authorization:token f34e190a0fad8882e727dcf1c0da922b");
			httpWebRequest.Method = "POST";

            string _b64 = System.Convert.ToBase64String(File.ReadAllBytes(imagepath));

            using (var streamWriter = new StreamWriter (httpWebRequest.GetRequestStream ())) {
				string json = "{\"full_name\":\"" + _firstname.ToUpper() + "\",\"mobile\":\"" + _number + "\",\"email\":\"" + _email + "\",\"image_blob\":\"" + _b64 + "\",\"shoe_size\":\"" + _size + "\"}";
                print(json);
				streamWriter.Write (json);
				streamWriter.Flush ();
				streamWriter.Close ();
			}

			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse ();
			using (var streamReader = new StreamReader (httpResponse.GetResponseStream ())) {
				var data = streamReader.ReadToEnd ();
				return data;
			}

		} catch (System.Exception e) {
            // Your catch here
            MainManager.Instance.AlertError("Error submitting form");
            logger.WriteToLogFile(e.ToString() + "Error submitting form");

            return string.Empty;
		}
	}


    /// <summary>
	/// Submit user details
	/// </summary>
	public string AddScore(int _score, int _timeInSec, int _userID)
    {
        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ipaddress + "score.php");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Headers.Add("Authorization:token f34e190a0fad8882e727dcf1c0da922b");

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"score\":\"" + _score + "\", \"time\":\"" + _timeInSec + "\", \"id\":\"" + _userID + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var data = streamReader.ReadToEnd();
                return data;
            }

        }
        catch (System.Exception e)
        {
            MainManager.Instance.AlertError("Error adding user score");

            // Your catch here
            logger.WriteToLogFile(e.ToString() + "Error adding user score");

            return string.Empty;
        }
    }



    /// <summary>
	/// Get list of spirits
	/// </summary>
	public string GetHighScore()
    {
        string message;

        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ipaddress + "highscore.php");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Headers.Add("Authorization:token f34e190a0fad8882e727dcf1c0da922b");

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                //				var data = streamReader.ReadToEnd().Split(',');
                message = streamReader.ReadToEnd();
            }
            return message;

        }
        catch (System.Exception e)
        {
            // Your catch here
            string errorMsg = "[A - 002] : " + "Database error: Could not find highscore " + e.ToString();
            logger.WriteToLogFile(errorMsg);
            return string.Empty;
        }
    }

}
