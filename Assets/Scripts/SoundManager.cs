﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    private static SoundManager _instance;

    public static SoundManager Instance { get { return _instance; } }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public AudioSource munch;
    public AudioSource dead;
    public AudioSource ghost;



    public void PlaySoundMunch()
    {
        munch.Play();
    }



    public void PlaySoundDead()
    {
        dead.Play();
    }


    public void PlaySoundEatingGhost()
    {
        ghost.Play();
    }
}
